<?php
$connect = new mysqli('localhost','root','','php_work');

if (!isset($_COOKIE['email']) && ($_COOKIE['password']))
{
	echo "good cookie working";
}
else
{
    header('location: login_cookie.php');
}


if(isset($_POST['submit'])) {
    $name = $_POST['name'];
    $fname = $_POST['fname'];
    $gender = $_POST['gender'];
    $dob = $_POST['dob'];
    $phnumber = $_POST['phnumber'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $course = $_POST['course'];
    echo $course;

    $query = "insert into sachtech(name, fname, gender, dob, ph_number, email, password, course) values 
    ('$name','$fname','$gender','$dob','$phnumber','$email','$password','$course')";
            
    if($connect->query($query)){
        echo "Data Stored";
    } else {
        echo $connect->error;
    }

}


?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Document</title>
<link rel="stylesheet" href="css/bootstrap.css ">
<script src="jquery/jquery-3.4.1.js"></script>
<link rel="stylesheet" href="sachtech_reg.css">


</head>
<body style="background: #222;">

    

    <div class="container mt-4 bg-white">
        <div class="row">

            <div class="col-5" style="min-height: 100vh;">

            </div>

            <div class="col-7">

                <form method="POST" class="register-form" id="register-form">
                   <!-- logout -->
                   <div class="bg-warning text-center p-3">
        <a href="logout.php" class="btn btn-info text-white" name="logout">Logout</a>
    </div>
                   <!-- logout -->
                   
                   
                    <h2 class="text-center text-uppercase">Student Registration Form</h2>

                    <div class="">

                        <div class="form-group">
                            <label for="name">Name :</label>
                            <input type="text" class="d-block w-100 form-control" name="name" id="name" required />
                        </div>

                        <div class="form-group">
                            <label for="father_name">Father Name :</label>
                            <input type="text" class="d-block w-100 form-control" name="fname" id="father_name" required />
                        </div>
                    </div>

                <hr>
                    <div class="form-radio">
                        <label for="gender" class="radio-label pl-2">Gender :</label>

                            <input type="radio" name="gender" value="male" class="ml-5" id="male">
                            <label for="male" class="mr-5">Male</label>


                            <input type="radio" name="gender" value="female" id="female">
                            <label for="female">Female</label>

                    </div>
                <hr>
                    <div class="dob">
                        <label for="dob">Date of birth:</label>
                        <input type="date" name="dob" class="w-50 form-control">
                    </div>

                    <!-- phone number -->
                <div class="ph_number">
                    <label for="phone">Phone number:</label>
                    <input type="number" name="phnumber" class="form-control">
                </div>

                    <!-- Email -->
                    <div class="email">
                        <label for="email">Email:</label>
                        <input type="email" name="email" class="form-control">
                    </div>

                    <!-- Password -->
                    <div class="password form-group">
                        <label for="password">Password:</label>
                        <input type="password" id="password" name="password" class="form-control">
                    </div>

                    <!-- courses -->
                    <div class="course" class="form-control">
                        <label for="">Select Course:</label>
                        <select name="course" class="form-control" id="course">
                            <option value="none">--- Select your course ---</option>
                            <option value="PHP">PHP</option>
                            <option value="android">Android</option>
                            <option value="ios">ios</option>
                        </select>
                    </div>

                    <!-- submit -->
                    <input type="submit" name="submit" class="btn btn-danger mt-5 pl-4 pr-4 pt-2 pb-2 d-block mx-auto">
                </form>
            </div>
        </div>
    </div>

    <!-- Script -->
    <script>
        
    </script>
</body>
</html>
